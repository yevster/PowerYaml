﻿using System;
using System.Collections.Generic;
using System.Management.Automation;
using YamlDotNet.Serialization;
using YamlDotNet.RepresentationModel;
using System.IO;
using System.Linq;
using YamlDotNet.Serialization.NamingConventions;

namespace Synopsys.PowerYaml {
    public class Deserializer {
        public static IList<YamlDocument> FromFile(FileInfo file){
            using (var stream = file.OpenRead())
            using (var reader = new StreamReader(stream))
            {
                var yamlStream = new YamlStream();
                yamlStream.Load(reader);
                return yamlStream.Documents;
            }
        }

        public static IList<YamlDocument> FromString(string ymlString){
            using (var reader = new StringReader(ymlString)){
                var yamlStream = new YamlStream();
                yamlStream.Load(reader);
                return yamlStream.Documents;
            }
        }

        public static PSObject ToPSObject (YamlNode node) {
            

            // Individual scalars can be returned as dictionaries
            if (node.NodeType == YamlNodeType.Scalar) {
                if (string.IsNullOrWhiteSpace(node.Tag)){
                    return null;
                }
                return PSObject.AsPSObject(obj: new Dictionary<string, string>(){[node.Tag] = (node as YamlScalarNode).Value} );
            } 
            else  if (node.NodeType == YamlNodeType.Alias){
                throw new NotImplementedException("Alias nodes are not presently supported");
            }
           
           // Mapping nodes: Recurse to non-scalar children, assign as properties
            else if (node.NodeType == YamlNodeType.Mapping){
                var result = new PSObject();
                foreach (var child in (node as YamlMappingNode).Children){
                    if (child.Key.NodeType != YamlNodeType.Scalar){
                        throw new NotImplementedException("Non-scalar keys in mapping nodes not supported.");
                    }
                    var newProperty = new PSNoteProperty((child.Key as YamlScalarNode).Value, null);
                    
                    if (child.Value.NodeType == YamlNodeType.Scalar){
                        newProperty.Value = (child.Value as YamlScalarNode).Value;
                    } else {
                       newProperty.Value = ToPSObject(child.Value);
                    }
                    result.Properties.Add(newProperty);
                }
                return result;
            }

            //Serialize sequence as object array. Powershell will know what to do with it.
            else if (node.NodeType == YamlNodeType.Sequence){
                return PSObject.AsPSObject((node as YamlSequenceNode).Children.Select(childNode=>ToPSObject(childNode)).ToArray());
            }

            else { //Not gonna happen.
                throw new ArgumentException("Unknown node type: "+node.NodeType);
            }

        }
    }
}