. ${PSScriptRoot}/init.ps1

add-type -Path ${PSScriptRoot}/PowerYaml.dll  -ReferencedAssemblies ("${PSScriptRoot}/YamlDotNet.dll", "${PSScriptRoot}/Newtonsoft.Json.dll")
. ${PSScriptRoot}/ConvertFrom-Yaml.ps1

