function ConvertFrom-Yaml {
    <#
     .SYNOPSIS
     Deserializes YAML into a dynamic object
     .DESCRIPTION
     
     .EXAMPLE      
    #>

    [OutputType([PSObject])]
    Param(
        # The YAML string to be read
        [Parameter(HelpMessage = 'The YAML to be deserialized', ValueFromPipeline = $true, ParameterSetName = "asString", Mandatory=$true)]
        [string] $Yaml,

        # The file to be read
        [Parameter(HelpMessage = 'The YAML file to be deserialized', ValueFromPipeline=$true, ParameterSetName="asFile", Mandatory=$true)]
        [System.IO.FileInfo] $File
    )

    Begin {
        if ($File){
            return Get-Content -Raw -LiteralPath $File.FullName | ConvertFrom-Yaml
        }
    }

    Process {
        if ($Yaml){
            Write-Debug "Converting $Yaml"
            $docs = [Synopsys.PowerYaml.Deserializer]::FromString($Yaml)
            return $docs | ForEach-Object { [Synopsys.PowerYaml.Deserializer]::ToPSObject($_.RootNode) }
        }
    }
}